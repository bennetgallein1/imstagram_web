<?php
/**
 * Created by PhpStorm.
 * User: bennet
 * Date: 23.10.18
 * Time: 22:56
 */

namespace Controllers;


use Angle\Engine\RouterEngine\Collection;
use Angle\Engine\RouterEngine\Router;
use Angle\Engine\Template\Engine;
use Module\BaseModule\BaseModule;
use Safe\Exceptions\FilesystemException;

class Panel {

    private $engine, $collection, $database, $navmanager, $permissionmanager;

    private static $instance;

    /**
     * Panel constructor.
     * @throws FilesystemException
     */
    public function __construct() {
        $this->engine = new Engine("_views");
        $this->collection = new Collection();


        $config = file_get_contents("config.json");
        $config = json_decode($config);
        foreach ($config as $key => $value) {
            define($key, $value);
        }


        self::setInstance($this);

        $this->initBaseRoutes();

        // NavigationManager
        new DataBase(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

        // initiate all Modules
        new BaseModule();


        $this->match();
    }

    private function initBaseRoutes() {
        $engine = $this->engine;
    }

    private function match() {
        $router = new Router($this->collection);
        $route = $router->matchCurrentRequest();
        if (!$route) {
            $this->engine->render("_views/404.tmp");
        }
    }

    /**
     * @return Engine
     */
    public static function getEngine() {
        return self::$instance->engine;
    }

    /**
     * @return Collection
     */
    public static function getCollection() {
        return self::$instance->collection;
    }

    /**
     * @return DataBase
     */
    public static function getDatabase() {
        return self::$instance->database;
    }

    /**
     * @param $newdatabase
     */
    public static function setDatabase($newdatabase) {
        self::$instance->database = $newdatabase;
    }

    /**
     * @param $instance Panel
     */
    public static function setInstance($instance) {
        self::$instance = $instance;
    }

    /**
     * @return Panel
     */
    public static function getInstance() {
        return self::$instance;
    }

}