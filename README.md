# Imstagran Schulprojekt

Inhaltsverzeichniss: 
- Dateistruktur
- Installation nötiger Software
- Einrichten
- Testen

## Dateistruktur
Das Projekt ist in verschiedene Ordner aufgeteilt, da wir ein MVC System nutzen.
In `_Controllers` finden sich alle Klassen die für das Framework nötig sind.
- `DataBase.php` - Verbindung mit der Datenbank
- `Panel.php` - Einstiegspunkt in die Software

In `_Modules` findet sich das Modulare System des Framework, welches die Module beinhaltet. Für unser kleines Projekt nutzten wir nur den Ordner `BaseModule`.
In diesem befinden sich die Datein `BaseModule.php` und `module.json`, wobei die `module.json` nicht von Interesse ist, da sie erst ab mehreren Modulen interessant ist.
In der Datei `BaseModule.php` finden wir die Klasse `BaseModule`, welche die Klasse `Module` erweitert (zu finden in dem `_Modules` Ordner).
Hier haben wir 2 Funktionen und Variablen welche Metadatan beinhalten.
- `__construct()`:  Initialisiert das Modul im Framework mit dem Autor, dem Modulnamen und der Version.
- `routes()`: ist zum registrieren der Routen im Framework verantwortlich und verweist in zweiten Parameter immer auf einen Absoluten Klassenpfad, welcher dann statisch vom Framework aufgerufen wird, sobald es einen passenden Pfad findet. Zusätzlich können noch Parameter in der URL übergeben werden, wie an `/:profile` oder `/api/like/:id` zu sehen ist. Hier sind `profile` und `id` Parameter.
Diese Routen werden dann an die `Module` Klasse weiter gegeben und dort formal registriert.

In dem Ordner `_Modules/BaseModule/Controllers` finden sich die Klassen die zu dem passenden Modul die Funktionalität reinbringen.
Dort sehen wir erstmal nur `Dashboard.php` und `API.php`.

Im `_views` Ordner finden wir die ganzen Frontend-Sachen. Dort sind die Datein, welche vom Framework mit der Templateengine gerendert werden um ihren Inhalt aus der Datenbank zu bekommen.
Dort gilt eine klassische Aufteilung wie in jedem Frontend Projekt, .css Datein sind in `css`, und Bilder in `imgs`.
Der Feed von Imstagran finden wir in der `feed.html`. Beim öffnen dieser Datei fällt schnell auf, dass komische Schleifen vorhanden sind (`{ foreach :post in :posts }`).
Dies ist das Produkt der oben genannten Templateengine. Das ist einfach die Syntax die genutzt wird. Eine Übersicht der Syntax lässt sich [hier](https://github.com/bennetgallein/AngleFramework/blob/master/src/TemplateEngine/Syntax.php#L22) finden.

In der Datei `profile.html` sieht das ganze ziemlich genauso aus, nur das Design ist etwas anders.

Der Ordner `_Objects` enthält keine wichtigen Datein für dieses Projekt und ist ein Relikt des Frameworks.

### Zusammenfassung
Nur die Datein im Ordner `_Modules` und `_views` sind wichtig für das Projekt. Der Rest bildet sich aus den Datein des Frameworks.

## Installation nötiger Software
Bedingt durch die Tatsache, dass dieses Projekt auf PHP läuft, benötigt es einen Webserver welcher PHP unterstützt.
Unter Windows gibt es XAMPP unter Linux/OSX apache2/httpd oder nginx. Bei der Installation des Webservers sollte drauf geachtet werden, dass mindestens PHP7.2 installiert wird. Optimal ist PHP7.3, aber 7.2 wird unterstützt.
Des weiteren benötigen wir eine MySQL Datenbank, hier empfehlen wir MariaDB10.1 mindestens! Andere Versionen und Datenbanktypen sollten funktionieren, sind aber nicht getestet.
Zusätzlich braucht man noch composer, ein Abhängigkeitsmanagment tool für PHP. Zum Download gibt es das ganze unter [getcomposer.org](https://getcomposer.org)


Nach dem installieren der benötigten Software kann das Projekt entpackt werden. Unter Windows ist der Pfad für den XAMPP Ordner: `C:/xampp/htdocs/html`. Unter Linux `/var/www/html`

Wenn sich das Projekt in einem Unterordner auf dem Webserver befindet, muss in der `config.json` Datei im Stammverzeichnis der Projektes dieser `APP_URL` zugewiesen werden.

Nun müssen die Abhängigkeiten mit composer installiert werden. Dazu im Terminal in den Ordner des Projektes navigieren und `composer install && composer dump-autoload -o` ausführen. Nach einer kurzen Ladezeit sollten alle Sachen installiert sein.

Danach kann man in der `config.json` die Datenbankdaten eintragen. Danach kann die Datenbank über die Console importiert werden.
Wenn phpMyAdmin auf dem System installiert ist, kann der Content der `db.sql` direkt in die `SQL` Box bei phpMyAdmin kopiert werden.
Andernfalls muss man `mysql -u root -p <datenbankname> > db.sql` in der Console ausführen.

### Mögliche Probleme
Häufige Probleme/Fehler:
Bei der ersten Installation können durchaus Fehler auftreten, man hat nicht immer alles auf dem Schirm.

- Ich kann die Datenbank nicht importieren Lösung: Stell sicher dass die Datenbank schon existiert. Das Skript ist nicht in der Lage die Datenbank selbst zu erstellen, es kann nur Tabellen importieren.
Datenbank funktioniert, aber ich bekomme SQL Errors. Lösung: Dies liegt meist an einer falschen Datenbank. Manche meiner Software funktioniert nur mit MariaDB, welches dann installiert werden muss.
- Error 500 Internal Server Error. Lösung: Dies kann mehrere Ursachen haben:
führe "composer install && composer dump-autoload -o" aus
- Schau in den Apache2 logs nach Fehlermeldungen
- stell sicher mod_rewrite ist aktiv ("a2endmod rewrite")
- stell sicher .htaccess sind erlaubt (in der "/etc/apache2/apache2.conf" ist ein Bereich mit Verzeichnisen wo du den Documentroot findest . Ändere "AllowOverride None" zu "AllowOverride All" dort)
- 404 Page "/login" not found. (oder jegliche andere Seite). Lösung: Stell sicher, dass nichts von dem oberen den Fehler löst. Check die apache2 configs, dies ist meist ein Konfigurationsproblem. Guck auch in der "config.json" ob die "APP_URL" richtig gesetzt ist.
- Immer ordentlich lesen! Meistens stellt die Fehlermeldung das Problem richtig da und es lässt sich einfach lösen