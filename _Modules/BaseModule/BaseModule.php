<?php
/**
 * Created by PhpStorm.
 * User: bennet
 * Date: 02.11.18
 * Time: 09:47
 */

namespace Module\BaseModule;

use Module\Module;
use Objects\NavPoint;
use Objects\Permission;
use Safe\Exceptions\JsonException;
use Safe\Exceptions\PcreException;

class BaseModule extends Module {
    private $name = "BaseModule";
    private $version = "0.0.1";
    private $author = "Bennet Gallein <me@bennetgallein.de>";

    /**
     * UserModule constructor.
     * @throws \Safe\Exceptions\FilesystemException
     */
    public function __construct() {
        // registering Module in Framework via Parent Handler
        parent::__construct($this->name, $this->version, $this->author);
        // calling #routes function in this class to register routes
        $this->routes();
    }

    /**
     * Register all Routes to the Framework
     */
    public function routes() {
        // array with routes
        /*
         * array("/", "BaseModule\Controllers\Dashboard::main", ["engine" => $this->engine], "GET")
         *        ^                     ^                           ^            ^             ^
         *        |                     |                           |            |             |
         *       Path       Controller Class and function   function parameter -> value      HTTP Request Method
         *
         */
        $data = array(
            array("/", "BaseModule\Controllers\Dashboard::main", ["engine" => $this->engine], "GET"),
            array("/login", "BaseModule\Controllers\Authentication::login", ["engine" => $this->engine], "GET"),
            array("/login", "BaseModule\Controllers\Authentication::loginPost", [], "POST"),
            array("/register", "BaseModule\Controllers\Authentication::register", ["engine" => $this->engine], "GET"),
            array("/register", "BaseModule\Controllers\Authentication::registerPost", [], "POST"),
            array("/:profile", "BaseModule\Controllers\Dashboard::profile", ["engine" => $this->engine, "profile" => "\w+"], "GET"),
            array("/post", "BaseModule\Controllers\Dashboard::newPost", [], "POST"),
            array("/api/like/:id", "BaseModule\Controllers\API::like", ["id" => "\d+"], "GET"),
            array("/api/follow/:id", "BaseModule\Controllers\API::follow", ["id" => "\d+"], "GET"),
            array("/api/comment/:id", "BaseModule\Controllers\API::comment", ["id" => "\d+"], "POST")

        );
        // call to parent class to register routes
        $this->_registerRoutes($data);
    }
}