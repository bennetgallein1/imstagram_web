<?php
/**
 * Created by PhpStorm.
 * User: bennet
 * Date: 18.02.19
 * Time: 10:16
 */

namespace Module\BaseModule\Controllers;


use Angle\Engine\Template\Engine;
use Controllers\Panel;
use PDO;

class Dashboard {

    /**
     * Renders the feed. Needs a Join query to get data across tables
     * @param Engine $engine
     */
    public static function main(Engine $engine) {
        // get Database class from Framework to query for data
        $db = Panel::getDatabase();
        if (!isset($_SESSION['im_id'])) {
            header("Location: " . APP_URL . "login");
        }

        $following = $db->fetch_multi_row("following", array("user_id"), array("follower_id" => $_SESSION['im_id']));
        // get all posts and their poster's name from the database with a INNER JOIN SQL Query
        $posts = [];
        foreach($following as $followin) {
            $post = $db->custom_query("SELECT posts.*, accounts.name FROM posts INNER JOIN accounts ON posts.poster_id = accounts.id WHERE poster_id = ? OR poster_id = " . $_SESSION['im_id'] . " ORDER BY posts.id DESC ", array("poster_id" => $followin->user_id));
            $posts = $post->fetchAll();
        }
        for ($i = 0; $i < sizeof($posts); $i++) {
            $like = $db->custom_query("SELECT * FROM likes WHERE post_id=? AND user_id=?", array($posts[$i]['id'], $_SESSION['im_id']));
            $posts[$i]['like'] = $like->rowCount();
            $posts[$i]['comments'] = $db->custom_query("SELECT comments.*, accounts.name FROM comments INNER JOIN accounts ON comments.poster_id = accounts.id WHERE post_id=?", array("post_id" => $posts[$i]['id']));
        }
        // render data to file "_views/feed.html". $posts is the array of posts that will be rendered into the pages

        $engine->render("_views/feed.html", array(
            "posts" => $posts,
            "account" => $_SESSION['im_name']
        ));
    }

    /**
     * Render a specific profile
     * @param Engine $engine render engine tut tut
     * @param $profile the profile to get
     */
    public static function profile(Engine $engine, $profile) {
        // get Database class from Framework to query for data
        $db = Panel::getDatabase();

        // get single account from the database that matches the functions parameter $profile
        $profile = $db->fetch_single_row("accounts", "name", $profile, PDO::FETCH_ASSOC);
        // get all posts from that account.
        $posts = $db->fetch_multi_row("posts", array("file"), array("poster_id" => $profile['id']), PDO::FETCH_ASSOC);

        $is_following = $db->custom_query("SELECT * FROM following WHERE user_id= ? AND follower_id= ?", array("user_id" => $profile['id'], "follower_id" => $_SESSION['im_id']))->rowCount() == 0 ? false : true;
        // render data to file. Parameters are here:
        // "profile", which is the profile of the account
        // "posts", which are the posts of the account
        // "post_count" which is just the number of rows of the $posts query
        $engine->render("_views/profile.html", array(
            "profile" => $profile,
            "posts" => $posts,
            "post_count" => $posts->rowCount(),
            "following" => $is_following
        ));
    }

    /**
     * This is the part for posting a new picture
     */
    public static function newPost() {
        // get the current working directory aka, where the entry file (index.php) was executed
        $currentDir = getcwd();
        // set the upload directory which will be in the _views folder
        $uploadDirectory = "/_views/imgs/posts/";
        // just a variable to save errors
        $errors = []; // Store all foreseen and unforseen errors here
        // allowed file extensions for uploaded files
        $fileExtensions = ['jpeg', 'jpg', 'png', 'gif']; // Get all the file extensions

        // get the file name
        $fileName = $_FILES['file']['name'];
        // get the file size
        $fileSize = $_FILES['file']['size'];
        // get the temporary name in which the file is saved in /tmp
        $fileTmpName = $_FILES['file']['tmp_name'];
        // the the file type
        $fileType = $_FILES['file']['type'];
        // get the file Ending by making an array out of the name.  test.png => array("test", "png")
        $te = explode('.', $fileName);
        // get the last item of the array (png)
        $tem = end($te);
        // lowercase every char in the ending
        $fileExtension = strtolower($tem);

        // put together the path. Example: /var/www/html/imstagram/_views/imgs/posts/test.png
        $uploadPath = $currentDir . $uploadDirectory . basename($fileName);

        if (isset($_POST['submit'])) {
            // check if the file extension is allowed.
            if (!in_array($fileExtension, $fileExtensions)) {
                $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
            }
            // check if the File is bigger than 2mb
            if ($fileSize > 2000000) {
                $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
            }
            // make sure there are no errors until now
            if (empty($errors)) {
                // move the uploaded file from the temporary folder where PHP stores it by default to our designated location $uploadPath
                $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
                // if the move was successful
                if ($didUpload) {
                    echo "The file " . basename($fileName) . " has been uploaded";

                    // upload success;
                    // get Database from Framework to do some magic
                    $db = Panel::getDatabase();
                    /* insert data fro a new post into the database:
                     * file -> is the filename in the folder
                     * caption -> is the small text under each image
                     * likes -> the amount of likes the post has
                     * poster_id -> the Database ID of the poster
                     *
                    */
                    $db->insert("posts", array(
                        "file" => $fileName,
                        "caption" => $_POST['caption'],
                        "likes" => 0,
                        "poster_id" => $_SESSION['im_id']
                    ));
                    // redirect user back to feed
                    header("Location: " . APP_URL);
                } else {
                    // if an error accoured echo this messages
                    echo "An error occurred somewhere. Try again or contact the admin";
                }
            } else {
                // echo each error
                foreach ($errors as $error) {
                    echo $error . "These are the errors" . "\n";
                }
            }
        }

    }
}