<?php
/**
 * Created by PhpStorm.
 * User: bennet
 * Date: 28.02.19
 * Time: 16:09
 */

namespace Module\BaseModule\Controllers;


use Angle\Engine\Template\Engine;
use Controllers\Panel;

class Authentication {

    public static function login(Engine $engine) {
        $engine->render("_views/login.html");
    }

    public static function register(Engine $engine) {
        $engine->render("_views/register.html");
    }

    public static function loginPost() {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $db = Panel::getDatabase();

        $user = $db->fetch_single_row("accounts", "name", $username);

        if (!$user) {
            die("User not found!");
        }

        if (!password_verify($password, $user->password)) {
            die("Wrong password!");
        }

        $_SESSION['im_id'] = $user->id;
        $_SESSION['im_name'] = $user->name;

        header("Location: " . APP_URL . "");
    }

    public static function registerPost() {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $db = Panel::getDatabase();

        $db->insert("accounts", array(
            "name" => $username,
            "password" => password_hash($password, PASSWORD_DEFAULT),
            "file" => "test.jpg",
            "followers" => 0,
            "following" => 0
        ));
        header("Location: " . APP_URL . "login");
    }
}