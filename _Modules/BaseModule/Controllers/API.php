<?php
/**
 * Created by PhpStorm.
 * User: bennet
 * Date: 26.02.19
 * Time: 18:22
 */

namespace Module\BaseModule\Controllers;


use Controllers\Panel;

class API {

    public static function like($id) {
        $db = Panel::getDatabase();
        $post = $db->fetch_single_row("posts", "id", $id);

        $db->update("posts", array("likes" => $post->likes + 1), "id", $id);

        $db->insert("likes", array(
            "user_id" => $_SESSION['im_id'],
            "post_id" => $id
        ));
    }

    public static function follow($id) {
        $db = Panel::getDatabase();
        $acc = $db->fetch_single_row("accounts", "id", $id);

        $db->update("accounts", array("followers" => $acc->followers + 1), "id", $id);

        $uacc = $db->fetch_single_row("accounts", "id", $_SESSION['im_id']);

        $db->update("accounts", array("following" => $uacc->following + 1), "id", $_SESSION['im_id']);

        $db->insert("following", array(
            "follower_id" => $_SESSION['im_id'],
            "user_id" => $id
        ));
    }
    public static function comment($id) {
        $db = Panel::getDatabase();

        $db->insert("comments", array(
            "post_id" => $id,
            "poster_id" => $_SESSION['im_id'],
            "comment" => $_POST['comment']
        ));
    }
}