<?php

use Controllers\Panel;
use Tracy\Debugger;

require("vendor/autoload.php");

// start session
session_start();

// enable debugger for easier errors and set development mode manually
Debugger::enable(Debugger::DEVELOPMENT);
try {
    // launch framework instance.
    $p = new Panel();
} catch (\Safe\Exceptions\FilesystemException $e) {
    print($e->getTrace());
}